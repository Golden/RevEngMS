<?php
	if (session_status() == PHP_SESSION_NONE)
		session_start();

	include '../php/msheader.php';

	if (!isset($_POST["serverid"])) {
		http_response_code(400);
		echo "No 'serverid' in request";
		die();
	}

	$serverid = $_POST["serverid"];

	if (!filter_var($serverid, FILTER_VALIDATE_INT)) {
		http_response_code(400);
		echo "Server ID not an integer";
		die();
	}

	$sql = sprintf('SELECT * FROM ms_servers WHERE sid = %d LIMIT 1', $serverid);
	$result = $conn->query($sql);

	$ip = "";
	$port = "";

	if ($result->num_rows > 0) {
		global $ip, $port;
		
		$row = $result->fetch_assoc();

		$ip = $row["ip"];
		$port = $row["port"];
	} else {
		http_response_code(404);
		echo "Server not found";
		die();
	}

	//echo $ip . ":" . $port;

	if (!$query->Ask($ip, $port)) {
		http_response_code(404);
		echo "Server error occured";
		die();
	}
	
	$addr = "";
	$array = $query->Info($addr);

	if (!$array) {
		http_response_code(404);
		echo "Server not online";
		die();
	} else {
		header("Content-Type: text/json");
		echo json_encode($array, JSON_FORCE_OBJECT|JSON_THROW_ON_ERROR|JSON_PARTIAL_OUTPUT_ON_ERROR);
	}
?>