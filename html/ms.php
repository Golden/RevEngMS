<?php
	if (session_status() == PHP_SESSION_NONE)
		session_start();

	include '../php/msheader.php';

	$connectioninfo = "Place connection information here!"
?>

<!DOCTYPE html>
<html>
<head>
	<title>Custom SRB2 Master Server</title>
	<meta name="description" content="Check here to know who's currently playing!
		<?php echo $connectioninfo ?>">
	<style>
		table, td, th {
			text-align: center;
			padding: 0 15px;
			border: 1px solid black; 
			border-collapse: collapse;
		}
	</style>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
		crossorigin="anonymous">
	</script>
	<script>
		function query(id) {
			var str = "";
			$.post("msapi.php", {serverid: id}, function(data, status) {
				if (status != "OK")
					str = data.responseText;
				else
					str = data;

				$("#query").html(JSON.stringify(data));
			})
		}
	</script>
</head>
	<body>
<?php
	exec("pgrep ". $processname, $output, $return);
	if ($return != 0)
	    echo "		<span><b>Master Server is offline.</b></span><br>\n";
	else
		echo "		<span><b>Master Server is online.</b></span><br>\n";

	echo "		<span>" . preg_replace("/\R/", "</span><br/>\n		<span>", $connectioninfo) . "</span><br>\n";
?>

		<div id="query"></div>

		<span><b>Servers currently online:</b></span>

		<table style="margin-bottom: 15px">
			<thead>
				<tr>
					<th>Server ID</th>
					<th>IP</th>
					<th>Port</th>
					<th>Name</th>
					<th>Version</th>
					<th>Last Update</th>
				</tr>
			</thead>
			<tbody>
<?php
	$sql = "SELECT * FROM ms_servers WHERE upnow = 1";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while ($row = $result->fetch_assoc()) {
			echo "				<tr>\n";
			//echo "					<td>\n";
			//echo "						<button onclick=\"query(" . $row["sid"]. ")\">Query</button>\n";
			//echo "					</td>\n";
			//echo "					<td>" . $row["ip"]. "</td>\n";
			echo "					<td>" . $row["sid"] . "</td>\n";
			echo "					<td>\n";
			echo "						<a href=\"srb2://" . $row["ip"] . "\">" . $row["ip"] . "</a>\n";
			echo "					</td>\n";
			echo "					<td>" . $row["port"] . "</td>\n";
			echo "					<td style=\"background-color: #444444; color: #ffffff\">" . $query->Colorize($row["name"]) . "</td>\n";
			echo "					<td>" . $row["version"] . "</td>\n";
			echo "					<td>" . date("m/d/Y h:i:sA T", intval($row["timestamp"])). "</td>\n";
			echo "				</tr>\n";
		}
		//echo $result->num_rows. " results";
	} else {
		echo "				<tr>\n";
		echo "					<td colspan=\"7\"><b>No servers are currently online.</b></td>\n";
		echo "				</tr>\n";
	}
?>
			</tbody>
		</table>

		<span><b>Last 10 servers:</b></span>

		<table>
			<thead>
				<tr>
					<th>Server ID</th>
					<th>IP</th>
					<th>Port</th>
					<th>Name</th>
					<th>Version</th>
					<th>Last Update</th>
				</tr>
			</thead>
			<tbody>
<?php
	$sql = "SELECT * FROM ms_servers WHERE upnow = 0 LIMIT 10";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while ($row = $result->fetch_assoc()) {
			echo "				<tr>\n";
			echo "					<td>" . $row["sid"]. "</td>\n";
			echo "					<td>" . $row["ip"]. "</td>\n";
			echo "					<td>" . $row["port"]. "</td>\n";
			echo "					<td style=\"background-color: #444444; color: #ffffff\">" . $query->Colorize($row["name"]). "</td>\n";
			echo "					<td>" . $row["version"]. "</td>\n";
			echo "					<td>" . date("m/d/Y h:i:sA T", intval($row["timestamp"])). "</td>\n";
			echo "				</tr>\n";
		}
	} else {
		echo "				<tr>\n";
		echo "					<td colspan=\"6\"><b>Server log was recently cleared!</b></td>\n";
		echo "				</tr>\n";
	}
	$conn->close();
?>
			</tbody>
		</table>
	</body>
</html>
