<?php
if (session_status() == PHP_SESSION_NONE)
	session_start();

include 'SRB2Query/srb2query.php';

$query = new SRB2Query;

$processname = "server";

$servername = "localhost";
$username = "srb2_ms";
$password = "PASSWORD_CHANGEME";
$dbname = "srb2_ms";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
?>