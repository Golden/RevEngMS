# RevEngMS
### A reverse engineered Master Server.

This Master Server was clean-room reverse engineered from James's Master Server (currently hosted [here](https://mb.srb2.org/MS/0 "James's Master Server").)
A live version can be found [here](https://goldentails.tk/ms "Golden's Master Server").

#### Dependencies
- Apache (with `libapache2-mod-php7.3` or `libapache2-mod-php`)
- PHP (with `php7.3-mysql` or `php-mysql`)
- MySQL (MariaDB in MySQL compatibility mode works too)

There might be other depends that I've forgotten.

#### Installation
1. Create a SQL user — a good default is `srb2_ms`@`localhost` — and give it a password. Remember the username, hostname, and password you choose for later.
2. Execute the included `structure.sql` via a SQL user with database creation/modification permissions. It will create the database `srb2_ms` and not touch any other databases. (If you're unsure, check the file itself!)
3. Grant the new account created in Step 1 full privileges to the database `srb2_ms`.
4. Fill the credentials from Step 1 into `php/msheader.php` so that your Master Server can read and write to the `srb2_ms` database.
5. Copy the `html` and `php` folders into your Apache web server's equivalent of `/var/www` or `/srv`. Make sure to merge `html/.htaccess` with any existing `.htaccess` you have, otherwise users will see the raw PHP code of `html/ms`!

Afterwards, you should have a working Master Server!
You can now modify the `srb2_ms` database to add or remove rooms, ban or unban players, unlist servers, etc.